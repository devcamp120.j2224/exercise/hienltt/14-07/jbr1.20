public class App {
    public static void main(String[] args) throws Exception {
        //khởi tạo đối tượng hính chữ nhật rectangle (không tham số)
        Rectangle rectangle1 = new Rectangle();
        //khởi tạo đối tượng hình chữ nhật rectangle2 (có tham số)
        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());
        double dienTich1 = rectangle1.getArea();
        double dienTich2 = rectangle2.getArea();
        double chuVi1 = rectangle1.getPerimeter();
        double chuVi2 = rectangle2.getPerimeter();
        System.out.println("Diện tích của đối tượng rectangle1 = " + dienTich1 + ",chu vi 1 = "+ chuVi1);
        System.out.println("Diện tích của đối tượng rectangle2 = " + dienTich2 + ",chu vi 2 = "+ chuVi2);
    }
}
